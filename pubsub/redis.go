package pubsub

import (
	"fmt"
	"log"
	"github.com/gomodule/redigo/redis"
)

type Config struct {
	Network		string		`json:"network"`
	Address		string  	`json:"address"`
	SubChannels []string 	`json:"subChannels"`
}

type PubSub struct {
	Connection		*redis.PubSubConn
	SubChannel		chan string
}

// return a pubsub connection to redis
func GetPubSubConnection (config *Config) *PubSub {
	c, err := redis.Dial(config.Network, config.Address)
	if err != nil {
		log.Fatalf("Fail to connect to Redis: %v\n", err)
	}
	psc := redis.PubSubConn{Conn: c}
	for _, name := range config.SubChannels {
		psc.Subscribe(name)
		//fmt.Printf("Send Subscribe for channel: %v\n", name)
	}
	out := make(chan string)
	return &PubSub{Connection: &psc, SubChannel:out}
}

// Listen for incoming message from the channel we subscribed to
// (should run within a go routine)
// only one channel for now
func (ps *PubSub) Listen() {
	for {
		switch v := ps.Connection.Receive().(type) {
		case redis.Message:
			//ps.SubChannel <- fmt.Sprintf("%s: message: %s", v.Channel, v.Data)
			ps.SubChannel <- fmt.Sprintf("%s", v.Data)
		case redis.Subscription:
			log.Printf("%s to %s\n", v.Kind, v.Channel)
		}
	}
}