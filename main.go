package main

import (
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"gitlab.com/clem77/pizza/config"
	"gitlab.com/clem77/pizza/dao"
	"gitlab.com/clem77/pizza/pubsub"
	"gitlab.com/clem77/pizza/handlers"
)

func main()  {
	conf := config.LoadConfig()

	// -------------------
	// ------- DAO -------

	// init the dao (use postgres database)
	pizzaDao := dao.Connect(&conf.Dao)
	defer pizzaDao.DbConnection.Close()
	log.Println("Init 1/3 - Connect to DB [√]")

	// -------------------
	// ----- PUBSUB ------

	// init Handlers for PubSub messages
	subHandlers := handlers.GetSubHandlers(&pizzaDao)
	// init pubsub
	ps := pubsub.GetPubSubConnection(&conf.PubSub)
	defer ps.Connection.Close()
	go ps.Listen()
	go func() {
		for  {
			jsonMessage := <-ps.SubChannel
			err := subHandlers.UpdateIngredient(jsonMessage)
			if err != nil {
				log.Fatal(err)
			}
		}
	}()
	log.Println("Init 2/3 - Connect to Redis [√]")

	// -------------------
	// ------- API -------

	// init Handlers for HTTP requests
	apiHandlers := handlers.GetApiHandlers(&pizzaDao)
	// init the router
	router := mux.NewRouter()
	router.HandleFunc("/api/ping", apiHandlers.Ping).Methods("GET")
	router.HandleFunc("/api/pizza/all", apiHandlers.GetAllPizza).Methods("GET")
	router.HandleFunc("/api/pizza/{id}", apiHandlers.GetPizzaById).Methods("GET")
	router.HandleFunc("/api/pizza/", apiHandlers.CreatePizza).Methods("POST")
	router.HandleFunc("/api/pizza/{id}", apiHandlers.UpdatePizza).Methods("PUT")
	router.HandleFunc("/api/pizza/{id}", apiHandlers.DeletePizza).Methods("DELETE")

	log.Println("Init 3/3 - Http Router set [√]")
	log.Fatal(http.ListenAndServe(conf.HttpServerAddress, router))
}