-- CREATE USER docker;
-- CREATE DATABASE docker;
-- GRANT ALL PRIVILEGES ON DATABASE docker TO dockerenv;

CREATE TABLE ingredient (
  ingredient_id  serial PRIMARY KEY  -- implicit primary key constraint
, name           text NOT NULL UNIQUE
, available      boolean NOT NULL DEFAULT FALSE
);

CREATE TABLE pizza (
  pizza_id   serial PRIMARY KEY  -- implicit primary key constraint
, name       text NOT NULL
, available  boolean NOT NULL DEFAULT FALSE
);

CREATE TABLE pizza_ingredient (
  pizza_id       int REFERENCES pizza (pizza_id) ON UPDATE CASCADE ON DELETE CASCADE
, ingredient_id  int REFERENCES ingredient (ingredient_id) ON UPDATE CASCADE
, CONSTRAINT pizza_ingredient_pkey PRIMARY KEY (pizza_id, ingredient_id)  -- explicit pk
);

INSERT INTO pizza (name) VALUES ('regina');
INSERT INTO pizza (name) VALUES ('obama');
INSERT INTO pizza (name) VALUES ('margarita') RETURNING pizza_id;

INSERT INTO ingredient (name) VALUES ('tomato');
INSERT INTO ingredient (name) VALUES ('cheese');
INSERT INTO ingredient (name) VALUES ('flour');
INSERT INTO ingredient (name) VALUES ('ham');
INSERT INTO ingredient (name) VALUES ('mushroom') RETURNING ingredient_id;

INSERT INTO pizza_ingredient (pizza_id, ingredient_id) VALUES (1, 1);
INSERT INTO pizza_ingredient (pizza_id, ingredient_id) VALUES (1, 2);
INSERT INTO pizza_ingredient (pizza_id, ingredient_id) VALUES (1, 3);
INSERT INTO pizza_ingredient (pizza_id, ingredient_id) VALUES (2, 1);
INSERT INTO pizza_ingredient (pizza_id, ingredient_id) VALUES (2, 4);
INSERT INTO pizza_ingredient (pizza_id, ingredient_id) VALUES (2, 5);
INSERT INTO pizza_ingredient (pizza_id, ingredient_id) VALUES (3, 3);
