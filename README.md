# Pizza Management Service

Microservice to manage pizza composition and availability

## Introduction:

Client applications and admins interact with the service through a REST API.
The data are store in a PostgreSQL database. The service keep its data up-to-date (e.g. ingredient availabilty) by subscribing to redis pubsub channel.
[Diagrams available here](https://gitlab.com/clem77/pizza/tree/master/docs)


## Goals of the service:

* Manage list of pizzas
* Manage pizza ingredients
* Monitor pizza availability

## API Routes:

###### - Common

* GET /api/ping --> to check that the service is running
* GET /api/pizza/all --> to get the list of all the pizza
* GET /api/pizza/:id --> to get the detail of a pizza

###### - Admin ONLY

* POST /api/pizza --> to create a new pizza
* PUT /api/pizza/:id --> to modify the name and content of a pizza
* DELETE /api/pizza/:id --> to delete a pizza

## Redis Pubsub

In the current version the service only subscribes to one channel to receive update on ingredient quantity.

When the quantity in stock of one ingredient changes, an update is published on the redis channel by the Stock Management Service, so the Pizza Management Service can update the availability on its side.


## How to try it?

First get the repo

	go get -u gitlab.com/clem77/pizza

Then get the dependencies

    go get -u "github.com/gorilla/mux"
    go get -u "github.com/gomodule/redigo/redis"
    go get -u "github.com/lib/pq"

Then start postgresql and redis using docker-compose:

	docker-compose -f docker-compose-test-env.yml up

Finally build and run

	go build && ./pizza

## Tests

At the moment only one unit-test in handlers


## What is missing?

The implementationis not complete. Here is a list of what is missing or not completely finished:

* Implement or finish to implement of insert, update, delete
* Update ingredient and pizza availability when message from redis is received (to finish)
* Unit tests for all the routes, handlers and dao
* Proper documentation for the router (Swagger or similar)
* recover from disconnection
* setup CI