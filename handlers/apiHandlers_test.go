package handlers

import (
	"testing"
	"net/http"
	"net/http/httptest"
)

func TestPing(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/Ping", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	apiHandlerTest := ApiHandler{}
	handler := http.HandlerFunc(apiHandlerTest.Ping)

	handler.ServeHTTP(rr, req)

	// Check the status
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body
	expected := `{"status":"ok"}`
	if rr.Body.String() != expected {
		t.Errorf("Wrong body: got %v want %v",
			rr.Body.String(), expected)
	}
}