package handlers

import (
	"fmt"
	"encoding/json"
)

type SubHandler struct {
	Dao		PizzaDAO // interface define in ap
	Pub		Publisher
}

type PubSubMessage struct {
	IngredientName	string	`:json:"ingredientName"`
	Quantity		string	`:json:"quantity"`
} 

// return an Handler for message received via pubsib channels
func GetSubHandlers(dao PizzaDAO) *SubHandler {
	return &SubHandler{Dao:dao}
}

func (h *SubHandler) UpdateIngredient(jsonMessage string) error {
	// parse json message
	var msg PubSubMessage
	err := json.Unmarshal([]byte(jsonMessage), &msg)
	if err != nil {
		return err
	}

	// get the ingredient matching the name
	ingredient, err:= h.Dao.FindIngredientByName(msg.IngredientName)
	if err != nil {
		return err
	}

	// check if the availability need to be updated
	if msg.Quantity > "0" && ingredient.Available {
		return nil
	} else if msg.Quantity == "0" && !ingredient.Available {
		return nil
	}
	fmt.Printf("%v availability changed!", ingredient.Name)

	// TODO: update ingredient availability

	// TODO: update pizza availability

	return nil
}