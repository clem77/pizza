package handlers

import (
	. "gitlab.com/clem77/pizza/models"
)

type PizzaDAO interface {
	FindAll() ([]PizzaBase, error)
	FindById(id int) (PizzaDescription, error)
	Insert(pizza Pizza) error
	Update(pizza Pizza) error
	Delete(id int) error
	FindIngredientByName(name string) (Ingredient, error)
}

type Publisher interface {
	PublishUpdate() error
}