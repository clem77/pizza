package handlers

import (
	"net/http"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	. "gitlab.com/clem77/pizza/models"
	"strconv"
	"log"
)

type ApiHandler struct {
	Dao		PizzaDAO
	Pub		Publisher
}

func GetApiHandlers(dao PizzaDAO) *ApiHandler {
	return &ApiHandler{Dao:dao}
}

func (h *ApiHandler) Ping(w http.ResponseWriter, r *http.Request) {
	log.Println("request::Ping")
	respondWithJson(w, 200, map[string]string{"status": "ok"})
}

func (h *ApiHandler) GetAllPizza(w http.ResponseWriter, r *http.Request) {
	log.Println("request::GetAllPizza")
	pizzas ,err := h.Dao.FindAll()
	//fmt.Println(pizzas)
	if err != nil {
		log.Fatal(err)
		respondWithError(w, 500, "Internal error")
		return
	}
	respondWithJson(w, 200, pizzas)
}

func (h *ApiHandler) GetPizzaById(w http.ResponseWriter, r *http.Request)  {
	log.Println("request::GetPizzaById")
	params := mux.Vars(r) // Get params
	pizzaId, err := strconv.Atoi(params["id"])
	if err != nil {
		respondWithError(w, 400, "Invalid id")
		return
	}
	pizza ,err := h.Dao.FindById(pizzaId)
	//fmt.Println(pizza)
	if err != nil {
		respondWithError(w, 500, "Internal error")
		return
	}
	if pizza.ID == 0 {
		respondWithError(w, 404, "No pizza matching id")
		return
	}
	respondWithJson(w, 200, pizza)
}

// Create a new pizza
// TODO finish
func (h *ApiHandler) CreatePizza(w http.ResponseWriter, r *http.Request)  {
	log.Println("request::CreatePizza")
	defer r.Body.Close()
	var pizzaDescription PizzaDescription
	if err := json.NewDecoder(r.Body).Decode(&pizzaDescription); err != nil {
		respondWithError(w, 400, "Invalid id")
		return
	}
	fmt.Println(pizzaDescription)

	respondWithJson(w, 201, pizzaDescription)
}

// Update pizza
// TODO implement
func (h *ApiHandler) UpdatePizza(w http.ResponseWriter, r *http.Request)  {
	log.Println("request::UpdatePizza")
	respondWithError(w, 501, "Not finish to implement yet")
}

// Delete pizza
// TODO implement
func (h *ApiHandler) DeletePizza(w http.ResponseWriter, r *http.Request)  {
	params := mux.Vars(r) // Get params
	pizzaId, err := strconv.Atoi(params["id"])
	if err != nil {
		respondWithError(w, 400, "Invalid id")
		return
	}
	err = h.Dao.Delete(pizzaId)
	if err != nil {
		respondWithError(w, 500, "Internal error")
		return
	}
	respondWithJson(w, 200, map[string]string{"status": "ok"})
}


// helper for error response
func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

// helper for ok response
func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}