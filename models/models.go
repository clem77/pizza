package models

type PizzaBase struct {
	ID         int     `json:"id"`
	Name       string  `json:"name"`
	Available  bool    `json:"available"`
}

type Ingredient struct {
	ID         int     `json:"id"`
	Name       string  `json:"name"`
	Available  bool    `json:"name"`
}

type PizzaDescription struct {
	PizzaBase
	IngredientList []string `json:"ingredientList,omitempty"`
}

type Pizza struct {
	PizzaBase
	Ingredients []Ingredient `json:"ingredients"`
}

// Update Pizza availability status if necessary
// return true is the availability of the pizza changed
func (p *Pizza) UpdateAvailability() (isDifferent bool){
	isAvailable := true
	for _, ingredient := range p.Ingredients{
		if ingredient.Available == false {
			p.Available = false
			break
		}
	}
	if p.Available != isAvailable {
		p.Available = isAvailable
		return true
	}
	return false
}

// Update Pizza availability status if necessary
// return true is the availability of the pizza changed
func (p *PizzaDescription) DescriptionToPizza() {

}