package dao

import (
	"fmt"
	. "gitlab.com/clem77/pizza/models"
	"database/sql"
	_ "github.com/lib/pq"
	"encoding/json"
)

type Config struct {
	Host		string	`json:"host"`
	Port		int		`json:"port"`
	User		string	`json:"user"`
	Password	string	`json:"password"`
	Dbname		string	`json:"dbname"`
}

type PizzaDAO struct {
	DbConfig 	 	*Config
	DbConnection	*sql.DB
}

// Establish a connection to database
func (c *Config) StringifyConfig() string {
	return fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		c.Host, c.Port, c.User, c.Password, c.Dbname)
}

// Establish a connection to database
func Connect(conf *Config) PizzaDAO{
	stringConf := conf.StringifyConfig()
	db, err := sql.Open("postgres", stringConf)
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		panic(err)
	}
	return PizzaDAO{DbConfig: conf, DbConnection: db}
}

// Find list of Pizza (without their ingredients)
func (p *PizzaDAO) FindAll() ([]PizzaBase, error) {
	var pizzaList []PizzaBase
	rows, err := p.DbConnection.Query(`
		SELECT
			pizza_id as "id",
			name,
			available
		FROM pizza`)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		pizza := PizzaBase{}
		err = rows.Scan(
			&pizza.ID,
			&pizza.Name,
			&pizza.Available,
		)
		if err != nil {
			return nil, err
		}
		pizzaList = append(pizzaList, pizza)
	}
	defer rows.Close()
	return pizzaList, err
}

// Find a pizza with the list of the ingredients names
func (p *PizzaDAO) FindById(id int) (pizza PizzaDescription, err error) {
	sqlStatement := `
		SELECT
			t.id,
			t.name,
			t.available,
			json_agg(iname)
		FROM (
			SELECT
		ingredient.name as iname,
			pizza.pizza_id as id,
			pizza.name as name,
			pizza.available as available
		FROM
		pizza,
			ingredient,
			pizza_ingredient
		WHERE
		pizza.pizza_id = pizza_ingredient.pizza_id
		AND ingredient.ingredient_id = pizza_ingredient.ingredient_id
		AND pizza.pizza_id = $1
		) t
		GROUP BY t.name, t.id, t.available`
	row := p.DbConnection.QueryRow(sqlStatement, id)
	var ingredientsRaw string
	err = row.Scan(
		&pizza.ID,
		&pizza.Name,
		&pizza.Available,
		&ingredientsRaw,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Println("Zero rows found")
			return pizza, nil
		} else {
			return pizza, err
		}
	}
	err = json.Unmarshal([]byte(ingredientsRaw), &pizza.IngredientList)
	if err != nil {
		return pizza, err
	}
	return pizza, nil
}

// Insert new a pizza
func (p *PizzaDAO) Insert(pizza Pizza) error {
	// insert in pizza table
	sqlInsertPizza := `INSERT INTO pizza(name)
	VALUES($1) RETURNING pizza_id`
	err := p.DbConnection.QueryRow(sqlInsertPizza, pizza.Name).Scan(&pizza.ID)
	if err != nil {
		return err
	}

	// insert link with all ingredients in pizza_ingredient table
	sqlInsertIngredients := `INSERT INTO pizza_ingredient(pizza_id, ingredient_id)
	VALUES($1, $2)`
	for _, ingredient := range pizza.Ingredients {
		p.DbConnection.QueryRow(sqlInsertIngredients, pizza.ID, ingredient.ID)
	}

	return nil
}

// Delete an existing pizza
func (p *PizzaDAO) Delete(id int) error {
	sqlDeletePizza := `DELETE FROM pizza WHERE pizza_id = $1`
	var deleteId string
	err := p.DbConnection.QueryRow(sqlDeletePizza, id).Scan(&deleteId)
	if err != nil {
		return err
	}
	return nil
}

// Update an existing pizza
func (p *PizzaDAO) Update(pizza Pizza) error {
	fmt.Println("NOT IMPLEMENTED")
	return nil
}

// Update an existing pizza
func (p *PizzaDAO) FindIngredientByName(name string) (ingredient Ingredient, err error) {
	sqlGetIngredient := `SELECT
			ingredient_id as "id",
			name,
			available
		FROM ingredient WHERE name = $1`
	err = p.DbConnection.QueryRow(sqlGetIngredient, name).Scan(&ingredient.ID, &ingredient.Name, &ingredient.Available)
	return ingredient, err
}
