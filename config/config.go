package config

import (
	"encoding/json"
	"os"
	"gitlab.com/clem77/pizza/pubsub"
	"gitlab.com/clem77/pizza/dao"
)


type Config struct {
	Dao					dao.Config 		`json:"dao"`
	PubSub				pubsub.Config 	`json:"pubsub"`
	HttpServerAddress 	string 			`json:"httpServerAddress"`
}

//LoadConfig loads configurations
func LoadConfig() (conf Config) {
	var configPath string
	if os.Getenv("ENVIRONMENT") == "PROD" {
		configPath = "./config/config.production.json"
	} else {
		configPath = "./config/config.development.json"
	}
	file, err := os.Open(configPath)
	if err != nil {
		panic(err)
	}
	if err := json.NewDecoder(file).Decode(&conf); err != nil {
		panic(err)
	}
	return conf
}
